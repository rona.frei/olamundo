import PostModelo from 'componentes/PostModelo';
import styles from './SobreMim.module.css'
import fotoCapa from 'assets/sobre_mim_capa.png'
import fotoSobreMim from 'assets/sobre_mim_foto.jpeg'

function SobreMim(){
    return (
        <h1>
            <PostModelo
                fotoCapa={fotoCapa} 
                titulo="Sobre Mim"
            >
                <h3 className={styles.subtitulo}>
                    Olá , sou Ronaldo!
                </h3>

                <img src={fotoSobreMim}
                     alt="Minha foto"
                     className={styles.fotoSobreMim}>
                </img>

                <p className={styles.paragrafo}>
                    Com mais de 20 anos de experiência em Análise e Desenvolvimento de Sistemas de Informação com foco em web atuando no modelo Full-Stack.
                </p>

                <p className={styles.paragrafo}>
                    Trabalhei para grandes empresas como Bradesco, Itaú, Raia Drogasil, Boa Vista SCPC, GetNet, entre outras.
                </p>

                <p className={styles.paragrafo}>
                    Sempre buscando me aperfeiçoar e aprender mais sobre o mundo de TI. 
                </p>
            </PostModelo>
        </h1>
    );
}

export default SobreMim;